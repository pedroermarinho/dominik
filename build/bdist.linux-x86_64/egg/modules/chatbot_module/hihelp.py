from app.mac import mac, signals
from googlesearch import search
from urllib.request import urlopen
from bs4 import BeautifulSoup
'''
Signals this module listents to:
1. When a message is received (signals.command_received)
==========================================================
'''
@signals.command_received.connect
def handle(message):
    if message.command == "hi":
        hi(message)
    elif message.command == "imagem":
        imagem(message)


'''
Actual module code
==========================================================
'''
def hi(message):
    who_name = message.who_name
    answer = "Hi " + who_name
    mac.send_message(answer, message.conversation)
    
def imagem(message):

    html = urlopen("https://duckduckgo.com/?q="+message.text+"&t=qupzilla&ia=images&iax=images")
    res = BeautifulSoup(html.read(), "html5lib")
    tags = res.findAll("img")
    result = tags.__getitem__(0)
    print(result)
    mac.send_image(result, message.conversation)
