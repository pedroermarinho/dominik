# modules/hi_module.py

from app.mac import mac, signals
from assbot.principal import Main
from modules.Vestibular import questoes
from assbot.Comandos import Comando
# from modules.q_academico_ifam.q_academico import notas
numbers = ["1⃣", "2⃣", "3⃣", "4⃣", "5⃣"]

cmds= Comando()
main = Main()

@signals.message_received.connect
def handle(message):
    name = message.who_name
    # try:
    #     print(message)
    # except:
    #     print("erro")

    
    pergunta = main.mensagem_bot_pergunta(message.text)  # função para transformar o texto e verificar sua entrada
    # print(pergunta)  # mostra na tela a perguta

    # if message.text.startswith("🏫"):
    #     parts = message.text.split('\n')
    #     if parts[0].text.startswith("🏫") and parts[1].text.startswith("🏫")
    #         parts[0]=parts[0].replace('\n', '')  # deletando as quebras de linha
    #         parts[1]=parts[1].replace('\n', '')  # deletando as quebras de linha
    #         print(notas(parts[0],parts[1]))
    #         mac.send_message(str(qnotas(parts[0],parts[1])), message.conversation)


    if message.command == "v":
        mac.send_message(str(questoes.questao(name)), message.conversation)
    elif "cmd_quiz" == cmds.comando(pergunta):
        mac.send_message(str(questoes.questao(name)), message.conversation)
    elif pergunta in numbers:
        mac.send_message(str(questoes.resposta(pergunta,name)), message.conversation)
    
    else:

        resposta = main.mensagem_bot_resposta(pergunta)  # função que captura a pegunta e gera uma resposta
        # print(resposta)# mostra na tela a resposta
        try:
            mac.send_message(str(resposta).format(str(name)), message.conversation)
        except:
            # mac.send_message("Avisamos que a Dominik  ainda está em fase de teste , a cada dia estará mais inteligente \n\n"+str(resposta), message.conversation)
            mac.send_message(str(resposta), message.conversation)
            # Can also send media
            #mac.send_image("path/to/image.png", message.conversation)
            #mac.send_video("path/to/video.mp4", message.conversation)
