import sys
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication,QDialog
from PyQt5.uic import loadUi
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from assbot.principal import Main
import threading


class interface(QDialog):

    model= QStandardItemModel()
    main = Main()
    main.treino()
    nativo = False
    
    def __init__(self):
        super(interface,self).__init__()
        loadUi('interface_qt5/tela_inicial.ui',self)
        self.setWindowTitle("Dominik")

         
        self.MensagemList.setModel(self. model)

        self.TelegramButton.clicked.connect(self.threadTelegram_on)
        self.WhatsappButton.clicked.connect(self.threadWhatsapp_on)
        self.NativoButton.clicked.connect(self.threadNativo_on)
        self.EnviarButton.clicked.connect(self.threadEnviar_on)

    def threadWhatsapp_on(self):
        print("threadWhatsapp_on")
        threading.Thread(target=self.Whatsapp_on).start()

    @pyqtSlot()
    def Whatsapp_on(self):
        print("WhatsApp")
    
    def threadTelegram_on(self):
        print("threadTelegram_on")
        threading.Thread(target=self.Telegram_on).start()
    
    @pyqtSlot()
    def Telegram_on(self):
        print("Telegram")

    def threadNativo_on(self):
        print("threadNativo_on")
        threading.Thread(target=self.Nativo_on).start()
    
    @pyqtSlot()
    def Nativo_on(self):
        print("Nativo")
        if self.nativo is False:
            self.nativo=True
        else:
            self.nativo=False


    def threadEnviar_on(self):
        print("threadEnviar_on")
        threading.Thread(target=self.Enviar_on).start()

    @pyqtSlot()
    def Enviar_on(self):
        print("enviar")
        if(self.nativo):
            mensagem=self.MensagemText.text()
            resposta=self.main.mensagem_bot_resposta(mensagem)
            item=QStandardItem("\nVocê: "+str(mensagem)+"\nDominik: "+str(resposta))
            self.model.appendRow(item)
            self.MensagemText.setText("")
        
        

    
        



print("ok")
app =QApplication(sys.argv)
widget=interface()
widget.show()
sys.exit(app.exec_())


