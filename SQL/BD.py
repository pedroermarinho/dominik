import pymysql
import os
import random
from assbot.questao import quest


class banco_de_dados():
    # Abrimos uma conexão com o banco de dados:
    
    conexao = pymysql.connect(host='localhost',db='DOMINIK', user='root', passwd='')
    
    # Cria um cursor:
    cursor = conexao.cursor()

    def add_piadas(self):
        try:
            for _arquivo in os.listdir('assbot/ass_diversos/piadas'):  # percorrer todos os arquivos na pasta chats
                if _arquivo.endswith(".txt"):
                    print(_arquivo)  # mostrar o nome do aquivo que esta sendo lido
                    linhas = open('assbot/ass_diversos/piadas' + _arquivo, 'r').readlines()  # vamos ler linhas
                    for linha in linhas:
                        linha = linha.replace('\n', '') 
                        self.cursor.execute("INSERT INTO piadas (piada) VALUES (\'"+linha+"\')") # Executa o comando:
                        self.conexao.commit() # Efetua um commit no banco de dados.
                    
                    shutil.move('assbot/ass_diversos/piadas' + _arquivo,
                                'assbot/ass_diversos/piadas/ja_treinados/')  # mover os arquivos ja treinados para outra pasta para que não sejam treinados novamente
        except:
            print("Erro função-> treino piada")
        
    def add_curiosidade(self):
        try:
            for _arquivo in os.listdir('assbot/ass_diversos/curiosidade'):  # percorrer todos os arquivos na pasta chats
                if _arquivo.endswith(".txt"):
                    print(_arquivo)  # mostrar o nome do aquivo que esta sendo lido
                    linhas = open('assbot/ass_diversos/curiosidade' + _arquivo, 'r').readlines()  # vamos ler linhas
                    for linha in linhas:
                        linha = linha.replace('\n', '') 
                        self.cursor.execute("INSERT INTO curiosidades (curiosidade) VALUES (\'"+linha+"\')")# Executa o comando:
                        self.conexao.commit() # Efetua um commit no banco de dados.

                    shutil.move('assbot/ass_diversos/curiosidade' + _arquivo,
                                'assbot/ass_diversos/curiosidade/ja_treinados/')  # mover os arquivos ja treinados para outra pasta para que não sejam treinados novamente
        except:
            print("Erro função-> treino curiosidade")
    

    def add_quiz(self):
        try:
            for _arquivo in os.listdir('assbot/quiz'):  # percorrer todos os arquivos na pasta chats    
                if _arquivo.endswith(".txt"):
                    print(_arquivo)  # mostrar o nome do aquivo que esta sendo lido
                    arquivo = open('assbot/quiz/'+_arquivo, 'r').readlines()
                    for comando in arquivo:  # percorendo todos os comandos
                        comando = comando.replace('\n', '')  # deletando as quebras de linha
                        parts = comando.split('||')  # separando mensaguem de comando // dicionario de comandos
                        self.cursor.execute("INSERT INTO quiz (pergunta,alternativa1,alternativa2,alternativa3,alternativa4,alternativa5,resposta) VALUES (\'"+parts[0]+"\',\'"+parts[1]+"\',\'"+parts[2]+"\',\'"+parts[3]+"\',\'"+parts[4]+"\',\'"+parts[5]+"\',\'"+parts[6]+"\',)")        
                        self.conexao.commit()# Efetua um commit no banco de dados.
                    shutil.move('assbot/quiz/'+ _arquivo,'assbot/quiz/ja_treinados/')  # mover os arquivos ja treinados para outra pasta para que não sejam treinados novamente
        except:
            print("Erro função-> treino quiz")



    def Numero_aleatorio_piada(self):
        try:
            self.cursor.execute("SELECT COUNT(*) FROM piadas")
            conts = self.cursor.fetchall()
            cont = 0
            for c in conts:
                cont = int (c)
                break
            return int(random.randint(0, cont- 1))
        except:
            print("erro numero aleatorio piada")
            return int(0)
    
    def Numero_aleatorio_quiz(self):
        try:
            self.cursor.execute("SELECT COUNT(*) FROM quiz")
            conts = self.cursor.fetchall()
            cont = 0
            for c in conts:
                cont = int (c)
                break
            return int(random.randint(0, cont- 1))
        except:
            print("erro numero aleatorio quiz")
            return int(0)
 
    def Numero_aleatorio_curiosidade(self):
        try:
            self.cursor.execute("SELECT COUNT(*) FROM curiosidades")
            conts = self.cursor.fetchall()
            cont = 0
            for c in conts:
                cont = int (c)
                break
            return int(random.randint(0, cont- 1))
        except:
            print("erro numero aleatorio curiosidade")
            return int(0)
 

    def get_piada(self,cod=Numero_aleatorio_piada()):
        try:
            self.cursor.execute("SELECT piada FROM piadas WHERE id = \'"+cod+"\'")
            
            results = self.cursor.fetchall()
            
            result=None
            for res in results:
                result = str(cod)+"->"+str(res)
                break

            return result
        except:
            print("erro get_piada")
            return None
    def get_curiosidade(self,cod=Numero_aleatorio_curiosidade()):
        try:
            self.cursor.execute("SELECT curiosidade FROM curiosidades WHERE id = \'"+cod+"\'")
            
            results = self.cursor.fetchall()
            
            result=None
            for res in results:
                result = str(cod)+"->"+str(res)
                break
            return result
        except:
            print("erro get_quiz")
            return None

    def get_quiz(self,cod=Numero_aleatorio_curiosidade()):
        quests = quest()
        try:
            self.cursor.execute("SELECT pergunta, alternativa1, alternativa2, alternativa3, alternativa4, alternativa5, resposta FROM quiz WHERE id = \'"+cod+"\'")
            
            results = self.cursor.fetchall()
            
        
            for parts in results:
                result = str(cod)+"->"+str(res)
                quests.set_pergunta(str(cod)+"->"+str(parts["pergunta"]))
                quests.set_questA(str(parts["alternativa1"]))
                quests.set_questB(str(parts["alternativa2"]))
                quests.set_questC(str(parts["alternativa3"]))
                quests.set_questD(str(parts["alternativa4"]))
                quests.set_questE(str(parts["alternativa5"]))
                quests.set_resposta(str(parts["resposta"]))
                break
            return quest.result
        except:
            print("erro get_quiz")
            return None
        
        

    def fechar_conexao(self):
        # Finaliza a conexão
        self.conexao.close()
