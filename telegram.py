from tokens.chave_telegram import chave_token
import telepot
import time
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import asyncio
import telepot
from telepot.aio.loop import MessageLoop
from telepot.aio.delegate import pave_event_space, per_chat_id, create_open


# global respot = 'a'
from assbot.principal import Main
from assbot.Palavras_Chave import Palavra_Chave
from assbot.dados_sql.BD import banco_de_dados
from assbot.Comandos import Comando



main = Main()
Palavra_Chave = Palavra_Chave()
base_de_dados = banco_de_dados()
cmds= Comando()

try:
    telegram = telepot.Bot(chave_token)  # endereço de acesso do bot
except:
    print('erro de conexão')


def recebendo_mensagem(msg):  # função que ira receber a mensaguem
    global user_id, chat_id, username

    content_type, chat_type, chat_id = telepot.glance(msg)
    # global respot
    texto = msg['text']  # captura somente o texto enviado
    if "cmd_quiz" == cmds.comando(str(texto)):
        obj = base_de_dados.get_quiz()

        Pergunta(msg,obj[0], obj[1], obj[2], obj[3], obj[4], obj[5],obj[6],obj[7])
    else:
        pergunta = main.mensagem_bot_pergunta(texto)  # função para transformar o texto e verificar sua entrada
        print(pergunta)  # mostra na tela a perguta
        resposta = main.mensagem_bot_resposta(pergunta)  # função que captura a pegunta e gera uma resposta
        print(resposta)  # mostra a resposta na tela
        tipo_mensagem, tipo_chat, Nome_ID = telepot.glance(msg)
        # funçao para retorna o tipo da mensagem , tipo do chat e o Id do usuario que necessario para enviar mensagem
        try:
            print('................................................................................................')
            print('. Tipo de mensagem\t\t', tipo_mensagem)
            print('. Tipo de chat\t\t\t', tipo_chat)
            print('. Chat ID\t\t\t\t', Nome_ID)
            print('. Nome:\t\t\t\t\t', msg['from']['first_name'], msg['from']['last_name'])
            print('. Dados', msg)
            print('................................................................................................')
            base_de_dados.add_base_de_usuarios(str(Nome_ID),str( str(msg['from']['first_name']) + ' ' + msg['from']['last_name'] ))
            
            
        except:
            print('Erro conteudo')

        enviado = False
        while (enviado == False):
            try:
                telegram.sendMessage(int(Nome_ID), str(resposta).format(str(msg['from']['first_name']) + ' ' + str(msg['from']['last_name'])) + '\'')  # função para enviar mensagem para ousuario , o prmeiro parametro serve é id (enndereço) do usuario e o segundo é a mensagem
                enviado = True
            except():
                print('Mensagem não enviada: tentando novamente')
                enviado = False


# ---------------------------------------------------------------------------------------------------------------------------------


def Pergunta(msg, quest='Teste', A='Teste A', B='Teste B', C='Teste C', D='Teste D', E='Teste E',Resposta='0',cod=0):
    global user_id, chat_id, username

    content_type, chat_type, chat_id = telepot.glance(msg)

    keyboard = InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text='A ) ' + A, callback_data='1||'+str(Resposta)+'||'+str(cod))],
        [InlineKeyboardButton(text='B ) ' + B, callback_data='2||'+str(Resposta)+'||'+str(cod))],
        [InlineKeyboardButton(text='C ) ' + C, callback_data='3||'+str(Resposta)+'||'+str(cod))],
        [InlineKeyboardButton(text='D ) ' + D, callback_data='4||'+str(Resposta)+'||'+str(cod))],
        [InlineKeyboardButton(text='E ) ' + E, callback_data='5||'+str(Resposta)+'||'+str(cod))]
    ])

    telegram.sendMessage(int(chat_id), str(quest), reply_markup=keyboard)


def Resposta(msg):
    global user_id, chat_id
    # content_type, chat_type, chat_id = telepot.glance(msg)
    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')

    parts = query_data.split('||') 
    query_data=parts[0]
    respot=parts[1]
    cod=int(parts[2])

# ------------------------------------------------------------------------------

    if query_data == '1':
        telegram.answerCallbackQuery(query_id, text='Você apertou A')
        
        if respot == '1':
            base_de_dados.add_pontucao_acetou(chat_id,int(str(cod)+str(1)))

            telegram.sendMessage(chat_id,str(cod)+') '+'Parabéns {}, você acertou 👏🏼👏🏼👏🏼 '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
            
        
        else:
            base_de_dados.add_pontucao_errou(chat_id,int(str(cod)+str(1)))

            telegram.sendMessage(chat_id,str(cod)+') '+ '😓 Poxa, não foi dessa vez {}!!!'.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
        
            
    
# ------------------------------------------------------------------------------

    elif query_data == '2':
        telegram.answerCallbackQuery(query_id, text='Você apertou B')
        
        if respot == '2':
            base_de_dados.add_pontucao_acetou(chat_id,int(str(cod)+str(2)))

            telegram.sendMessage(chat_id,str(cod)+') '+  'Parabéns {}, você acertou 👏🏼👏🏼👏🏼 '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
            
        
        else:
            base_de_dados.add_pontucao_errou(chat_id,int(str(cod)+str(2)))

            telegram.sendMessage(chat_id,str(cod)+') '+  '😓 Poxa, não foi dessa vez {}!!!'.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name']) +"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
            
            
    
# ------------------------------------------------------------------------------
    
    elif query_data == '3':
        telegram.answerCallbackQuery(query_id, text='Você apertou C')
        
        if respot == '3':
            base_de_dados.add_pontucao_acetou(chat_id,int(str(cod)+str(3)))

            telegram.sendMessage(chat_id, str(cod)+') '+ 'Parabéns {}, você acertou 👏🏼👏🏼👏🏼 '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
           
        else:
            base_de_dados.add_pontucao_errou(chat_id,int(str(cod)+str(3)))

            telegram.sendMessage(chat_id,str(cod)+') '+  '😓 Poxa, não foi dessa vez {}!!!  '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name']) +"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
        
# ------------------------------------------------------------------------------
    
    elif query_data == '4':
        telegram.answerCallbackQuery(query_id, text='Você apertou D')
        
        if respot == '4':
            base_de_dados.add_pontucao_acetou(chat_id,int(str(cod)+str(4)))

            telegram.sendMessage(chat_id,str(cod)+') '+  'Parabéns {}, você acertou 👏🏼👏🏼👏🏼 '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
            
        
        else:
            base_de_dados.add_pontucao_acetou(chat_id,int(str(cod)+str(4)))

            telegram.sendMessage(chat_id,str(cod)+') '+  '😓 Poxa, não foi dessa vez {}!!! '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
        
            
    
# ------------------------------------------------------------------------------
    
    elif query_data == '5':
        telegram.answerCallbackQuery(query_id, text='Você apertou E')
        
        if respot == '5':
            base_de_dados.add_pontucao_acetou(chat_id,int(str(cod)+str(5)))

            telegram.sendMessage(chat_id,str(cod)+') '+  'Parabéns {}, você acertou 👏🏼👏🏼👏🏼 '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name'])+"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
        
            
        
        else:

            base_de_dados.add_pontucao_errou(chat_id,int(str(cod)+str(5)))

            telegram.sendMessage(chat_id,str(cod)+') '+  '😓 Poxa, não foi dessa vez {}!!! '.format(
                msg['from']['first_name'] + ' ' + msg['from']['last_name']) +"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
        
            
    
# ------------------------------------------------------------------------------

    else:

        base_de_dados.add_pontucao_errou(chat_id,int(str(cod)+str(0)))

        telegram.sendMessage(chat_id,str(cod)+') '+  '😓 Poxa, não foi dessa vez {}!!!'.format(
            msg['from']['first_name'] + ' ' + msg['from']['last_name']) +"\n Sua pontuação é de "+base_de_dados.get_pontuacao(chat_id)+" pontos")
        


# ------------------------------------------------------------------------------

# telegram.setWebhook()


# --------------------------------------------------------------------------------------------------------------------------------------------

try:
    telegram.message_loop({'chat':recebendo_mensagem, 'callback_query': Resposta},run_forever='Ligado...').run_as_thread()  # funçao que fica esperando uma mensagem chega e tem como parametro a funcçao que será executada quando ela chega
except():
    print('Falha na conexão')
while True:  # isso fara com que o programa fique rodando e na feche
    time.sleep(10)