
from datetime import datetime
from assbot.Palavras_Chave import Palavra_Chave
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from assbot.dados_sql.BD import banco_de_dados


palavra_chave = Palavra_Chave()
base_de_dados = banco_de_dados()

class Comando():
    print('class comando')
    dicionario_cmd = {}  # criando um dicionario
    comandos = None

    try:
        comandos = open('assbot/cmds/comandos/comandos.txt', 'r').readlines()  # abrindo e lendo o arquivo onde esta o comando
    except():
        comandos = None
    if comandos is not None:
        for comando in comandos:  # percorendo todos os comandos
            comando = comando.replace('\n', '')  # deletando as quebras de linha
            parts = comando.split('||')  # separando mensaguem de comando // dicionario de comandos
            dicionario_cmd.update({parts[0]: parts[1]})  # colocados dentro da lista as mensagens e os comandos

    def comando(self, cmd):  # pasar o comando // função

        try:  # except para probrelmas
            result =process.extract(cmd,self.dicionario_cmd.keys(),scorer=fuzz.token_sort_ratio,limit=1)

            for y,i in result:
                comando = y
                confianca  =i

            print("comando->"+comando)
            print("nivel de confiança->"+str(confianca))

            if(int(confianca) > 90):
                return self.dicionario_cmd[comando]  # resultado do comando
            else:

                return None

        except:  # caso não tenha
            print("erro piada")
            return None  # retorna a nada

    def Lista_comandos(self):  # função para lista os comandos


        comandos = open('assbot/cmds/comandos_texto.txt', 'r').readlines()
        result=''
        for msg in comandos:
            result =result+str(msg)+"\n"
        # for k, v in self.dicionario_cmd.items():  # percorrendo os itens do dicionario
        #     result = result + k + '=>' + v + '\n'  # mostrar mensagem e comando
        return result

    def executar_cmd(self, cmd):  # função para execuatr os comandos
        if cmd == '/start':
            comandos = open('assbot/cmds/mensagem_start.txt', 'r').readlines()
            resultado = None
            for msg in comandos:
                resultado = msg
            return resultado
        if cmd == 'cmd_hora':
            data = datetime.now()
            return 'São ' + str(data.hour) + ' horas e ' + str(data.minute) + ' minutos'
        elif cmd == 'cmd_data':
            data = datetime.now()
            return 'Hoje é ' + str(data.day) + ' de ' + str(data.month)
        elif cmd == 'cmd_lista_comandos':
            return self.Lista_comandos()
        elif cmd == 'cmd_treinar':
            # main.treinar()
            return 'treinado'
        elif cmd == 'cmd_piadas':
            return base_de_dados.get_piada()
        elif cmd == 'cmd_Charadas':
            return palavra_chave.Charada()
        elif cmd == 'cmd_citacoes':
            return palavra_chave.citacao()
        elif cmd == 'cmd_curiosidades':
            return base_de_dados.get_curiosidade()
        else:
            return None